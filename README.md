# bash-spec-test
## A Hello World Bash Script

I wrote a really complex hello world script.  It takes arguments, or not, and basically prints hello world or some other text string.

## BATS testing Hello World

BATS tests exist inside the [tests](tests/) directory.  The test isn't complete, but covers the basic switches and expected behavior of the script.

To execute BATS and see if the tests pass or fail, execute:

```bash
$ bats tests/
```

Feel free to clone/fork tinker and add more stuff.

