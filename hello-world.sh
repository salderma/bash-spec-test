#!/bin/bash

usage () {
  echo "Usage: $0 [ -m <message> | -f <message file> ] [ -o <output file> ]" 1>&2
  exit 1
}

die () {
  echo "$@"
  exit 2
}

help () {
  echo 'This is the most complex hello world script ever written.'
  echo '  Simply call the script itself, or with one of the following options:'
  echo '    -h  print this message'
  echo '    -m  replace "Hello World!" with the supplied message string (optional), can not be used with -f'
  echo '    -f  replace "Hello World!" with message supplied in the given file (optional), can not be used with -m'
  echo '    -o  output "Hello World!" message to supplied file instead of STDOUT (optional)'
  echo '    -O  output "Hello World!" message to supplied file instead of STDOUT (optional), overwrites the file if it exists'
  exit 0
}

# set defaults
msg="Hello World!"
optm=false
optf=false
opto=false
optO=false

while getopts "hm:f:o:O:" o; do
  case "${o}" in
    h)
      help
      ;;
    m)
      ${optf} && die "$0: Option -m and -f are mutually exclusive"
      msg=${OPTARG}
      optm=true
      ;;
    f)
      ${optm} && die "$0: Options -m and -f are mutually exclusive"
      inputf=${OPTARG}
      # shellcheck disable=SC2015
      [ -r "${inputf}" ] && msg=$(cat "${inputf}") || die "$0: Permission denied accessing ${inputf}"
      optf=true
      ;;
    o)
      ${optO} && die "$0: Options -o and -O are mutually exclusive"
      outputf=${OPTARG}
      [ -w "$(dirname "${outputf}")" ] || die "$0: Permision denied writing ${outputf}"
      method=">>"
      opto=true
      ;;
    O)
      ${opto} && die "$0: Options -o and -O are mutually exclusive"
      outputf=${OPTARG}
      [ -w "$(dirname "${outputf}")" ] || die "$0: Permission denied writing ${outputf}"
      method=">"
      optO=true
      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND - 1))

if [ -n "${outputf}" ]; then
  line="printf '%s\n' \"${msg}\" ${method} ${outputf}"
else
  line="printf '%s\n' \"${msg}\""
fi

eval "${line}"

exit 0;
