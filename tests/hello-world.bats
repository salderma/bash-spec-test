#!/usr/bin/env bats
#

setup() {
  mkdir .bats
}

@test "no args prints hello message" {
  run ./hello-world.sh
  [ $status -eq 0 ]
  [ "${lines[0]}" == 'Hello World!' ]
}

@test "with -m message argument" {
  run ./hello-world.sh -m 'testing -m'
  [ $status -eq 0 ]
  [ "${lines[0]}" == 'testing -m' ]
}

@test "with -f test.file argument" {
  /bin/echo "testing -f test.file" > .bats/test.file

  run ./hello-world.sh -f .bats/test.file
  [ $status -eq 0 ]
  [ "${lines[0]}" == 'testing -f test.file' ]
}

@test "with -o test-output.file" {
  run ./hello-world.sh -o .bats/test-output.file
  [ $status -eq 0 ]

  run /bin/cat .bats/test-output.file
  [ $status -eq 0 ]
  [ "${lines[0]}" == 'Hello World!' ]
}

@test "with -h" {
  run ./hello-world.sh -h
  [ $status -eq 0 ]
  [ "${lines[0]}" == 'This is the most complex hello world script ever written.' ]
  [ "${#lines[@]}" -gt 3 ]
}

teardown() {
  rm -rf .bats/
}
